package com.s808.capmotest.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.s808.capmotest.exceptions.UnknownViewmodelException
import com.s808.capmotest.model.MainScreenModelImpl

class MainViewModelFactory(private val context: Context) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(MainScreenModelImpl(context.resources)) as T
        }
        throw UnknownViewmodelException()
    }
}
