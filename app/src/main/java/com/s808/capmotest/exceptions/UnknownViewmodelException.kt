package com.s808.capmotest.exceptions

import java.lang.RuntimeException

class UnknownViewmodelException : RuntimeException("Unknown ViewModel class")