package com.s808.capmotest.ui.composables.main

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.s808.capmotest.ui.theme.DarkDesaturatedBlue
import com.s808.capmotest.ui.theme.PaleBlue
import com.s808.capmotest.ui.theme.White
import com.s808.capmotest.viewmodel.MainViewModel

@Composable
fun SubmitButton(viewModel: MainViewModel) {
    val context = LocalContext.current
    Button(
        onClick = {
            viewModel.submitData(context)
        },
        colors = ButtonDefaults.textButtonColors(
            backgroundColor = DarkDesaturatedBlue,
            contentColor = White
        ),
        shape = RoundedCornerShape(50),
    ) {
        Text(
            text = "Start my trial",
            style = MaterialTheme.typography.button,
            color = PaleBlue,
            modifier = Modifier.padding(horizontal = 32.dp, vertical = 4.dp)
        )
    }
}
