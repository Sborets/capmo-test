package com.s808.capmotest.model

import com.s808.capmotest.domain.SubscriptionPlanUsDollars

/**
 * API to provide logic and data for [MainViewModel]
 */
interface MainScreenModel {
    /**
     * Size of a discount for an annual plan
     */
    val annualDiscount: Double

    /**
     * Get subscription plan based on slider state
     */
    fun getSubscriptionPlan(sliderValue: Float): SubscriptionPlanUsDollars

    /**
     * Returns title (String) to be displayed in [ChoosingCard]
     */
    fun getTitle(subscriptionPlan: SubscriptionPlanUsDollars): String

    /**
     * Returns price (formatted String) based on user choice (slider and annual toggle)
     */
    fun getPrice(
        subscriptionPlan: SubscriptionPlanUsDollars,
        annualDiscountEnabled: Boolean
    ): String
}