package com.s808.capmotest.ui.composables.main

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.s808.capmotest.R
import com.s808.capmotest.ui.composables.common.BriefHorizontalSpacer
import com.s808.capmotest.ui.theme.GrayishBlue

@Composable
fun DescriptionPointer(pointerText: String) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.padding(bottom = 8.dp)
    ) {
        Image(
            painter = painterResource(R.drawable.ic_icon_check),
            contentDescription = "pointer mark picture",
            modifier = Modifier
                .size(8.dp)
        )
        BriefHorizontalSpacer(16)
        Text(
            text = pointerText,
            style = MaterialTheme.typography.body2,
            color = GrayishBlue
        )
    }
}
