package com.s808.capmotest.model

import android.content.res.Resources
import com.s808.capmotest.R
import com.s808.capmotest.domain.SubscriptionPlanUsDollars
import com.s808.capmotest.domain.SubscriptionPlanUsDollars.*
import java.math.BigDecimal

class MainScreenModelImpl(private val resources: Resources) : MainScreenModel {
    override val annualDiscount: Double = 0.75

    override fun getSubscriptionPlan(sliderValue: Float): SubscriptionPlanUsDollars =
        when {
            sliderValue < 25 -> TEN_K
            sliderValue < 50 -> FIFTY_K
            sliderValue < 75 -> ONE_HUNDRED_K
            sliderValue < 100 -> FIVE_HUNDRED_K
            else -> ONE_M
        }

    override fun getTitle(subscriptionPlan: SubscriptionPlanUsDollars): String =
        if (subscriptionPlan == ONE_M) {
            String.format(resources.getString(R.string.mainscreen_title_m), 1)
        } else {
            String.format(
                resources.getString(R.string.mainscreen_title_k),
                subscriptionPlan.pageViews / 1000
            )
        }

    override fun getPrice(
        subscriptionPlan: SubscriptionPlanUsDollars,
        annualDiscountEnabled: Boolean
    ): String {
        val price = if (annualDiscountEnabled) {
            subscriptionPlan.price.multiply(BigDecimal.valueOf(annualDiscount))
        } else {
            subscriptionPlan.price
        }
        return "$${price.setScale(2).toPlainString()}"
    }
}
