package com.s808.capmotest

import android.content.res.Resources
import com.s808.capmotest.domain.SubscriptionPlanUsDollars
import com.s808.capmotest.model.MainScreenModelImpl
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import java.math.BigDecimal

@RunWith(MockitoJUnitRunner::class)
class MainScreenModelTest {

    private val res = mock<Resources> {
        on { getString(R.string.mainscreen_title_m) } doReturn "%1\$dM PAGEVIEWS"
        on { getString(R.string.mainscreen_title_k) } doReturn "%1\$dK PAGEVIEWS"
    }

    private val model = MainScreenModelImpl(res)

    @Test
    fun `model getSubscriptionPlan returns 10k plan on 0`() {
        val plan = model.getSubscriptionPlan(0f)
        assertEquals(plan, SubscriptionPlanUsDollars.TEN_K)
    }

    @Test
    fun `model getSubscriptionPlan returns 10k plan on 24`() {
        val plan = model.getSubscriptionPlan(24.5f)
        assertEquals(plan, SubscriptionPlanUsDollars.TEN_K)
    }

    @Test
    fun `10k plan has expected price`() {
        val plan = model.getSubscriptionPlan(24.5f)
        assertEquals(plan.price, BigDecimal(8))
    }

    @Test
    fun `model getSubscriptionPlan returns 50k plan on 25`() {
        val plan = model.getSubscriptionPlan(25f)
        assertEquals(plan, SubscriptionPlanUsDollars.FIFTY_K)
    }

    @Test
    fun `model getSubscriptionPlan returns 50k plan on 49`() {
        val plan = model.getSubscriptionPlan(49f)
        assertEquals(plan, SubscriptionPlanUsDollars.FIFTY_K)
    }

    @Test
    fun `50k plan has expected price`() {
        val plan = model.getSubscriptionPlan(49.5f)
        assertEquals(plan.price, BigDecimal(12))
    }

    @Test
    fun `model getSubscriptionPlan returns 100k plan on 50`() {
        val plan = model.getSubscriptionPlan(50f)
        assertEquals(plan, SubscriptionPlanUsDollars.ONE_HUNDRED_K)
    }

    @Test
    fun `model getSubscriptionPlan returns 100k plan on 74`() {
        val plan = model.getSubscriptionPlan(74f)
        assertEquals(plan, SubscriptionPlanUsDollars.ONE_HUNDRED_K)
    }

    @Test
    fun `100k plan has expected price`() {
        val plan = model.getSubscriptionPlan(74f)
        assertEquals(plan.price, BigDecimal(16))
    }

    @Test
    fun `model getSubscriptionPlan returns 500k plan on 75`() {
        val plan = model.getSubscriptionPlan(75f)
        assertEquals(plan, SubscriptionPlanUsDollars.FIVE_HUNDRED_K)
    }

    @Test
    fun `model getSubscriptionPlan returns 500k plan on 99`() {
        val plan = model.getSubscriptionPlan(99f)
        assertEquals(plan, SubscriptionPlanUsDollars.FIVE_HUNDRED_K)
    }

    @Test
    fun `500k plan has expected price`() {
        val plan = model.getSubscriptionPlan(99f)
        assertEquals(plan.price, BigDecimal(24))
    }

    @Test
    fun `model getSubscriptionPlan returns 1M plan on 100`() {
        val plan = model.getSubscriptionPlan(100f)
        assertEquals(plan, SubscriptionPlanUsDollars.ONE_M)
    }

    @Test
    fun `model getSubscriptionPlan returns 1M plan on 200`() {
        val plan = model.getSubscriptionPlan(200f)
        assertEquals(plan, SubscriptionPlanUsDollars.ONE_M)
    }

    @Test
    fun `1M plan has expected price`() {
        val plan = model.getSubscriptionPlan(200f)
        assertEquals(plan.price, BigDecimal(36))
    }

    @Test
    fun `model getTitle returns proper value for 10k`() {
        val title = model.getTitle(SubscriptionPlanUsDollars.TEN_K)
        assertEquals(title, "10K PAGEVIEWS")
    }

    @Test
    fun `model getPrice returns proper value for 10k monthly`() {
        val title = model.getPrice(SubscriptionPlanUsDollars.TEN_K, false)
        assertEquals(title, "\$8.00")
    }

    @Test
    fun `model getPrice returns proper value for 10k yearly`() {
        val title = model.getPrice(SubscriptionPlanUsDollars.TEN_K, true)
        assertEquals(title, "\$6.00")
    }

    @Test
    fun `model getTitle returns proper value for 50k`() {
        val title = model.getTitle(SubscriptionPlanUsDollars.FIFTY_K)
        assertEquals(title, "50K PAGEVIEWS")
    }


    @Test
    fun `model getPrice returns proper value for 50k monthly`() {
        val title = model.getPrice(SubscriptionPlanUsDollars.FIFTY_K, false)
        assertEquals(title, "\$12.00")
    }

    @Test
    fun `model getPrice returns proper value for 50k yearly`() {
        val title = model.getPrice(SubscriptionPlanUsDollars.FIFTY_K, true)
        assertEquals(title, "\$9.00")
    }

    @Test
    fun `model getTitle returns proper value for 100k`() {
        val title = model.getTitle(SubscriptionPlanUsDollars.ONE_HUNDRED_K)
        assertEquals(title, "100K PAGEVIEWS")
    }


    @Test
    fun `model getPrice returns proper value for 100k monthly`() {
        val title = model.getPrice(SubscriptionPlanUsDollars.ONE_HUNDRED_K, false)
        assertEquals(title, "\$16.00")
    }

    @Test
    fun `model getPrice returns proper value for 100k yearly`() {
        val title = model.getPrice(SubscriptionPlanUsDollars.ONE_HUNDRED_K, true)
        assertEquals(title, "\$12.00")
    }

    @Test
    fun `model getTitle returns proper value for 500k`() {
        val title = model.getTitle(SubscriptionPlanUsDollars.FIVE_HUNDRED_K)
        assertEquals(title, "500K PAGEVIEWS")
    }

    @Test
    fun `model getPrice returns proper value for 500k monthly`() {
        val title = model.getPrice(SubscriptionPlanUsDollars.FIVE_HUNDRED_K, false)
        assertEquals(title, "\$24.00")
    }

    @Test
    fun `model getPrice returns proper value for 500k yearly`() {
        val title = model.getPrice(SubscriptionPlanUsDollars.FIVE_HUNDRED_K, true)
        assertEquals(title, "\$18.00")
    }


    @Test
    fun `model getTitle returns proper value for 1m`() {
        val title = model.getTitle(SubscriptionPlanUsDollars.ONE_M)
        assertEquals(title, "1M PAGEVIEWS")
    }

    @Test
    fun `model getPrice returns proper value for 1m monthly`() {
        val title = model.getPrice(SubscriptionPlanUsDollars.ONE_M, false)
        assertEquals(title, "\$36.00")
    }

    @Test
    fun `model getPrice returns proper value for 1m yearly`() {
        val title = model.getPrice(SubscriptionPlanUsDollars.ONE_M, true)
        assertEquals(title, "\$27.00")
    }
}