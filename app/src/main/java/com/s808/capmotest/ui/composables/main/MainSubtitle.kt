package com.s808.capmotest.ui.composables.main

import androidx.compose.foundation.layout.Column
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.s808.capmotest.R
import com.s808.capmotest.ui.composables.common.BriefVerticalSpacer
import com.s808.capmotest.ui.theme.GrayishBlue

@Composable
fun MainSubtitle() {
    Column {
        Text(
            text = stringResource(R.string.main_screen_subtitle_1),
            style = MaterialTheme.typography.h2,
            color = GrayishBlue,
        )
        BriefVerticalSpacer(4)
        Text(
            text = stringResource(R.string.main_screen_subtitle_2),
            style = MaterialTheme.typography.h2,
            color = GrayishBlue,
        )
    }
}
