package com.s808.capmotest.ui.composables.main

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.s808.capmotest.R

@Composable
fun MainTitle() {
    Text(
        text = stringResource(R.string.main_screen_title),
        style = MaterialTheme.typography.h1,
    )
}
