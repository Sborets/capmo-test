package com.s808.capmotest.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val SoftCyan = Color(0xFFa5f3eb)
val StrongCyan = Color(0xFF10d5c2)
val LightGrayishRed = Color(0xFFfeece7)
val LightRed = Color(0xFFff8c66)
val PaleBlue = Color(0xFFbdccff)

val White = Color(0xFFFFFFFF)
val VeryPaleBlue = Color(0xFFfafbff)
val LightGrayishBlue = Color(0xFFeaeefb)
val LightGrayishBlue2 = Color(0xFFcdd7ee)
val GrayishBlue = Color(0xFF858fad)
val DarkDesaturatedBlue  = Color(0xFF293356)

