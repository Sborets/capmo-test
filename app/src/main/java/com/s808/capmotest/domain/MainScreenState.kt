package com.s808.capmotest.domain

/**
 * This class holds data to be displayed on main screen
 */
data class MainScreenState(
    val title: String,
    val price: String
)
