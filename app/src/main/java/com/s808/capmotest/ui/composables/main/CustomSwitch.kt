package com.s808.capmotest.ui.composables.main

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.s808.capmotest.ui.theme.LightGrayishBlue2
import com.s808.capmotest.ui.theme.StrongCyan
import com.s808.capmotest.ui.theme.White

@Composable
fun CustomSwitch(
    scale: Float = 2f,
    width: Dp = 24.dp,
    height: Dp = 12.dp,
    checkedTrackColor: Color = StrongCyan,
    uncheckedTrackColor: Color = LightGrayishBlue2,
    gapBetweenThumbAndTrackEdge: Dp = 2.dp,
    onCheckedChange: ((Boolean) -> Unit)? = null,
    checked: Boolean = false
) {
    val switchON = remember {
        mutableStateOf(checked)
    }
    val thumbRadius = (height / 2) - gapBetweenThumbAndTrackEdge
    val animatePosition = animateFloatAsState(
        targetValue = if (switchON.value)
            with(LocalDensity.current) { (width - thumbRadius - gapBetweenThumbAndTrackEdge).toPx() }
        else
            with(LocalDensity.current) { (thumbRadius + gapBetweenThumbAndTrackEdge).toPx() }
    )
    Canvas(
        modifier = Modifier
            .size(width = width, height = height)
            .scale(scale = scale)
            .pointerInput(Unit) {
                detectTapGestures(
                    onTap = {
                        switchON.value = !switchON.value
                        onCheckedChange?.invoke(switchON.value)
                    }
                )
            }
    ) {
        drawRoundRect(
            color = if (switchON.value) checkedTrackColor else uncheckedTrackColor,
            cornerRadius = CornerRadius(x = 10.dp.toPx(), y = 10.dp.toPx()),
        )
        drawCircle(
            color = White,
            radius = thumbRadius.toPx(),
            center = Offset(
                x = animatePosition.value,
                y = size.height / 2
            )
        )
    }
}
