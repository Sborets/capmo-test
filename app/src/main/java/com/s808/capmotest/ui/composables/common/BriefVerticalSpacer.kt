package com.s808.capmotest.ui.composables.common

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun BriefVerticalSpacer(height: Int) {
    Spacer(modifier = Modifier.height(height.dp))
}
