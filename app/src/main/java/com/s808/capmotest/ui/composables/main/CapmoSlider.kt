package com.s808.capmotest.ui.composables.main

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import com.s808.capmotest.R
import com.s808.capmotest.ui.theme.LightGrayishBlue
import com.s808.capmotest.ui.theme.SoftCyan
import com.s808.capmotest.ui.theme.StrongCyan
import com.s808.capmotest.viewmodel.MainViewModel

@Composable
fun CapmoSlider(viewModel: MainViewModel) {

    var sliderPosition by remember { mutableStateOf(0f) }

    SliderValueHorizontal(
        value = sliderPosition,
        onValueChange = { sliderPosition = it },
        valueRange = 0f..100f,
        onValueChangeFinished = {
            viewModel.updateSliderValue(sliderPosition)
        },
        steps = 3,
        thumb = { modifier, _: Dp, _, _, _ ->
            CustomThumb(modifier)
        },
        track = { modifier: Modifier,
                  progress: Float,
                  _, _, _ ->

            Box(
                Modifier.padding(vertical = 30.dp)
                    .height(8.dp).then(modifier)
            ) {
                val bgTrack = Modifier.background(
                    LightGrayishBlue,
                    RoundedCornerShape(48)
                )

                Spacer(bgTrack.fillMaxHeight().then(modifier))

                val bgProgress = Modifier.background(
                    SoftCyan,
                    RoundedCornerShape(48)
                )

                Spacer(
                    bgProgress.fillMaxHeight()
                        .fillMaxWidth(fraction = progress)
                        .then(modifier)
                )
            }
        },
        thumbSizeInDp = DpSize(41.dp, 41.dp)
    )
}

@Composable
fun CustomThumb(modifier: Modifier) {
    Box {
        Box(
            modifier = modifier
                .clip(CircleShape)
                .background(StrongCyan)
        )
        Image(
            painter = painterResource(R.drawable.ic_icon_slider),
            contentDescription = "",
            modifier = modifier
                .padding(9.dp)
        )
    }
}
