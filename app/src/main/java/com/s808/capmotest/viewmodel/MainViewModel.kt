package com.s808.capmotest.viewmodel

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.s808.capmotest.domain.MainScreenState
import com.s808.capmotest.domain.SubscriptionPlanUsDollars.TEN_K
import com.s808.capmotest.model.MainScreenModel

class MainViewModel(private val model: MainScreenModel) : ViewModel() {

    private var sliderValue: Float = 0f
    private var annualToggleEnabled = false
    private var _screenState = MutableLiveData(getInitialState())
    val screenState: LiveData<MainScreenState> = _screenState

    fun getInitialState(): MainScreenState = MainScreenState(
        title = model.getTitle(TEN_K),
        price = model.getPrice(TEN_K, annualToggleEnabled)
    )

    fun updateSliderValue(value: Float) {
        sliderValue = value
        updateView()
    }

    fun updateAnnualToggleValue(enabled: Boolean) {
        annualToggleEnabled = enabled
        updateView()
    }

    fun submitData(context: Context) {
        val subscriptionPlan = model.getSubscriptionPlan(sliderValue)
        Toast.makeText(
            context,
            "PLAN: ${subscriptionPlan.pageViews}, ANNUAL: $annualToggleEnabled",
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun updateView() {
        val subscriptionPlan = model.getSubscriptionPlan(sliderValue)
        val newState = MainScreenState(
            title = model.getTitle(subscriptionPlan),
            price = model.getPrice(subscriptionPlan, annualToggleEnabled)
        )
        _screenState.value = newState
    }
}
