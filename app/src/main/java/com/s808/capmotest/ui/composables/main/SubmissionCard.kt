package com.s808.capmotest.ui.composables.main

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import com.s808.capmotest.ui.composables.common.BriefVerticalSpacer
import com.s808.capmotest.ui.theme.White
import com.s808.capmotest.viewmodel.MainViewModel

@Composable
fun SubmissionCard(viewModel: MainViewModel) {
    Surface(
        color = White,
        elevation = 12.dp,
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 2.dp)
            .padding(horizontal = 32.dp)
            .clip(RoundedCornerShape(bottomEnd = 8.dp, bottomStart = 8.dp))
    ) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            BriefVerticalSpacer(24)
            DescriptionPointer("Unlimited websites")
            DescriptionPointer("100% data ownership")
            DescriptionPointer("Email reports")
            BriefVerticalSpacer(24)
            SubmitButton(viewModel)
            BriefVerticalSpacer(24)
        }
    }
}
