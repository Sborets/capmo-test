package com.s808.capmotest.ui.activity

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import com.s808.capmotest.viewmodel.MainViewModel
import com.s808.capmotest.viewmodel.MainViewModelFactory
import com.s808.capmotest.ui.composables.main.MainCapmoScreen
import com.s808.capmotest.ui.theme.CapmoTestTheme


class MainActivity : ComponentActivity() {
    private val mainViewModel by viewModels<MainViewModel> { MainViewModelFactory(applicationContext) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CapmoTestTheme {
                MainCapmoScreen(mainViewModel)
            }
        }
    }
}
