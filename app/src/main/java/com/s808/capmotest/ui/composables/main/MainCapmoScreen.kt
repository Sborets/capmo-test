package com.s808.capmotest.ui.composables.main

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.s808.capmotest.viewmodel.MainViewModel

@Composable
fun MainCapmoScreen(viewModel: MainViewModel) {
    Surface(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = 56.dp),

        color = MaterialTheme.colors.background
    ) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            TitleSection()
            ChoosingCard(viewModel)
            SubmissionCard(viewModel)
        }

    }
}
