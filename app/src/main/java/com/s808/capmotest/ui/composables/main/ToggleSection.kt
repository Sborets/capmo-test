package com.s808.capmotest.ui.composables.main

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.s808.capmotest.R
import com.s808.capmotest.ui.composables.common.BriefHorizontalSpacer
import com.s808.capmotest.ui.theme.GrayishBlue
import com.s808.capmotest.ui.theme.LightGrayishRed
import com.s808.capmotest.ui.theme.LightRed
import com.s808.capmotest.viewmodel.MainViewModel

@Composable
fun ToggleSection(viewModel: MainViewModel) {
    Row(verticalAlignment = Alignment.CenterVertically) {
        val checkedState = remember { mutableStateOf(false) }
        Text(
            text = stringResource(R.string.main_screen_monthly_title),
            style = MaterialTheme.typography.h6,
            color = GrayishBlue
        )
        BriefHorizontalSpacer(24)
        CustomSwitch(
            checked = checkedState.value,
            onCheckedChange = {
                checkedState.value = it
                viewModel.updateAnnualToggleValue(it)
            }
        )
        BriefHorizontalSpacer(24)
        Text(
            text = stringResource(R.string.main_screen_yearly_title),
            style = MaterialTheme.typography.h6,
            color = GrayishBlue
        )
        BriefHorizontalSpacer(8)
        Box(
            modifier = Modifier
                .clip(RoundedCornerShape(32.dp))
                .background(LightGrayishRed)
        ) {
            Text(
                text = stringResource(R.string.main_screen_discount_value),
                style = MaterialTheme.typography.caption,
                color = LightRed,
                modifier = Modifier
                    .padding(horizontal = 8.dp)
                    .padding(vertical = 4.dp)
            )
        }
    }
}
