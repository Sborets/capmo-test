package com.s808.capmotest.ui.composables.main

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.s808.capmotest.domain.MainScreenState
import com.s808.capmotest.viewmodel.MainViewModel
import com.s808.capmotest.R
import com.s808.capmotest.ui.theme.GrayishBlue

@Composable
fun PriceSection(viewModel: MainViewModel) {
    val state: MainScreenState by viewModel.screenState.observeAsState(viewModel.getInitialState())
    Row(verticalAlignment = Alignment.CenterVertically) {
        Text(
            text = state.price,
            style = MaterialTheme.typography.h4,

        )
        Spacer(modifier = Modifier.width(8.dp))
        Text(
            text = stringResource(R.string.main_screen_monthly_disclaimer),
            style = MaterialTheme.typography.h5,
            color = GrayishBlue
        )
    }
}
