package com.s808.capmotest.domain

import java.math.BigDecimal

/**
 * This class holds constant subscription values (pageviews and prices)
 * For US Dollars
 */
enum class SubscriptionPlanUsDollars (val pageViews: Int, val price: BigDecimal) {
    TEN_K(10000, BigDecimal(8)),
    FIFTY_K(50000, BigDecimal(12)),
    ONE_HUNDRED_K(100000, BigDecimal(16)),
    FIVE_HUNDRED_K(500000, BigDecimal(24)),
    ONE_M(1000000, BigDecimal(36))
}
