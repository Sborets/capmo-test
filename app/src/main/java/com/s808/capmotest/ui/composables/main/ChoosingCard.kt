package com.s808.capmotest.ui.composables.main

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import com.s808.capmotest.domain.MainScreenState
import com.s808.capmotest.ui.composables.common.BriefVerticalSpacer
import com.s808.capmotest.ui.theme.GrayishBlue
import com.s808.capmotest.ui.theme.White
import com.s808.capmotest.viewmodel.MainViewModel

@Composable
fun ChoosingCard(viewModel: MainViewModel) {
    Surface(
        color = White,
        modifier = Modifier
            .padding(top = 40.dp)
            .padding(horizontal = 32.dp)
            .clip(RoundedCornerShape(topStart = 8.dp, topEnd = 8.dp))

    ) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            val state: MainScreenState by viewModel.screenState.observeAsState(viewModel.getInitialState())
            Text(
                text = state.title,
                style = MaterialTheme.typography.h3,
                color = GrayishBlue,
                modifier = Modifier
                    .padding(top = 32.dp)

            )
            BriefVerticalSpacer(14)
            CapmoSlider(viewModel)
            BriefVerticalSpacer(12)
            PriceSection(viewModel)
            BriefVerticalSpacer(34)
            ToggleSection(viewModel)
            BriefVerticalSpacer(42)
        }
    }
}
